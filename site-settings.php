<?php

return [
    'api_version' => 2,
    'config_dir'  => 'novum.svb',
    'namespace'   => 'ApiNovumSvb',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'api.svb.demo.novum.nu',
    'test_domain' => 'api.test.svb.demo.novum.nu',
    'dev_domain'  => 'api.svb.innovatieapp.nl',
];
